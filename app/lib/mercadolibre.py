import json
import requests
from urllib.parse import urlencode


class Mercadolibre:

    AUTH_URL = 'https://auth.mercadolibre.com.ar'
    API_URL = 'https://api.mercadolibre.com'

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret

    def auth_url(self, redirect_uri):
        params = {
            'client_id': self.client_id,
            'response_type': 'code',
            'redirect_uri': redirect_uri,
        }

        return '{0}/authorization?{1}'.format(self.AUTH_URL, urlencode(params))

    def authorize(self, code, redirect_uri):
        params = {
            'grant_type': 'authorization_code',
            'client_id' : self.client_id,
            'client_secret' : self.client_secret,
            'code': code,
            'redirect_uri': redirect_uri,
        }
        headers = {
            'Accept': 'application/json',
            'Content-type':'application/json',
        }

        uri = '{0}/oauth/token'.format(self.API_URL)

        response = requests.post(uri, params=urlencode(params), headers=headers)

        if response.status_code == 200:
            response_info = response.json()
            self.access_token = response_info['access_token']
            self.user_id = response_info['user_id']
            return self.access_token

        return None

    def get(self, path, params={}):
        headers = {
            'Accept': 'application/json',
            'Content-type': 'application/json',
        }

        uri = '{0}{1}'.format(self.API_URL, path)

        response = requests.get(uri, params=urlencode(params), headers=headers)
        return response

    def post(self, path, body={}, params={}):
        headers = {
            'Accept': 'application/json',
            'Content-type': 'application/json',
        }

        uri = '{0}{1}'.format(self.API_URL, path)

        response = requests.post(uri, data=json.dumps(body), params=urlencode(params), headers=headers)
        return response
