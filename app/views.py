from django.conf import settings
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
)
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import (
    redirect,
    render,
)
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.generic import View

from app.lib.mercadolibre import Mercadolibre


User = get_user_model()

meli = Mercadolibre(client_id=settings.MELI_ID, client_secret=settings.MELI_SECRET)


class HomeView(View):
    """
    Initial home page. Here we list items for the logged in user.
    """

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        params = { 'access_token' : request.session.get('access_token') }
        response = meli.get(path='/users/{0}/items/search'.format(request.session.get('user_id')), params=params)
        res_json = response.json()
        kwargs['items'] = []
        for item_id in res_json['results']:
            item_response = meli.get(path='/items/{0}'.format(item_id), params=params)
            kwargs['items'].append(item_response.json())

        return render(request, 'home.html', kwargs)


class LoginView(View):
    """
    User login page.
    """

    def get(self, request, *args, **kwargs):
        return render(request, 'login.html', kwargs)


class MeliLoginView(View):
    """
    Initiate a redirect to a Mercadolibre endpoint which will display the login dialog.
    """

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('home')

        auth_url = meli.auth_url('http://localhost:8000/ingresar/meli/cb/')
        return redirect(auth_url)


class MeliLoginCallbackView(View):
    """
    Exchanging Code for an Access Token. Then create and login the User object.
    """

    def get(self, request, *args, **kwargs):
        try:
            meli.authorize(request.GET.get('code', ''), 'http://localhost:8000/ingresar/meli/cb/')

            params = { 'access_token': meli.access_token }
            response = meli.get(path='/users/{0}'.format(meli.user_id), params=params)

            res_json = response.json()

            if not res_json.get('email'):
                messages.error(request, _('Por problemas de permisos no pudimos obtener tu email.'))
                return redirect('home')

            user, created = User.objects.update_or_create(
                email=res_json['email'],
                defaults={
                    'username': res_json['nickname'],
                    'first_name': res_json['first_name'],
                    'last_name': res_json['last_name'],
                },
            )

            user = authenticate(nickname=user.username)
            if user is not None:
                login(request, user)

                # Store user ID and access_token inside session.
                request.session['user_id'] = meli.user_id
                request.session['access_token'] = meli.access_token
        except:
            messages.error(request, _('Hubo un error autenticando el usuario en ML.'))
            return redirect('home')

        return redirect('home')
