from django.contrib.auth import get_user_model


User = get_user_model()


class MeliBackend(object):

    def authenticate(self, nickname):
        try:
            return User.objects.get(username=nickname)
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None
