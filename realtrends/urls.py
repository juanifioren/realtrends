from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path

from app.views import *


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('ingresar/', LoginView.as_view(), name='login'),
    path('ingresar/meli/', MeliLoginView.as_view(), name='meli-login'),
    path('ingresar/meli/cb/', MeliLoginCallbackView.as_view(), name='meli-login-callback'),
    path('salir/', LogoutView.as_view(), name='logout'),
    path('admin/', admin.site.urls),
]
