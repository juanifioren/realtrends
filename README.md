
# RealTrends

## About

Stack used:

* Python 3.6.3
* Django 2.0.2

**Packages used**
I only use `django` and `requests`.

**Why not using official python lib for Meli API's?**
Well, basically because don't have support for Python 3.

## Installation

Setup project environment with [virtualenv](https://virtualenv.pypa.io) and [pip](https://pip.pypa.io).

```bash
$ virtualenv -p python3 project_env

$ source project_env/bin/activate

$ git clone https://bitbucket.org/juanifioren/realtrends.git
$ cd realtrends
$ pip install -r requirements.txt
```

Run your the app.

```bash
$ python manage.py migrate
$ python manage.py runserver
```

Open your browser and go to `http://localhost:8000`.

Run unit test.

```bash
$ python manage.py test
```

## Functionalities

* Barebone implementation of Mercadolibre SDK for Python 3.
* Login/registration of users using custom django auth backend.
* List user items after logged in using Meli API.
* ~~Create new item via Meli API.~~
* Close session.

## Things to improve

* Add setting redirect URL and remove hardcoded `localhost:8000`.
* Add loggers handling API fails.
* Add cache for items.
* Maybe move HomeView API logic to a custom helper class for Meli.

Created by Juan Ignacio Fiorentino. 08 Feb 2018.
